{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module AddMaterialView
    (
      addGet
    , addPost
    ) where

import           Text.Blaze.Html5
import qualified Text.Blaze.Html5 as H
import           Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5.Attributes as A
import           Text.Blaze.Html.Renderer.Text (renderHtml)

import           DefaultTemplateView

addGet = defaultedTemplate $
    H.div ! class_ "span9" $ do
        h2 "Add new reading material"
        H.form ! action "/add" ! method "post" $ do
            fieldset $ do
                legend "Reading material details"
                H.label ! A.for "title" $ "Title"
                input ! A.name "title" ! A.id "title" ! type_ "text"
                br
                H.label ! A.for "author" $ "Author"
                input ! A.name "author" ! A.id "author" ! type_ "text"
                br
                H.label ! A.for "link" $ "Link"
                input ! A.name "link" ! A.id "link" ! type_ "text"
                br
                input ! class_ "btn btn-primary"! type_ "submit" ! value "Add"

addPost title author link = defaultedTemplate $
    H.div ! class_ "span9" $ do
        h2 $ do
            "Added: "
            title
