{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DefaultTemplateView
    (
      template
    , defaultedTemplate
    , defaultSidebarContent
    ) where

import qualified Data.Text.Lazy as L
import           Text.Blaze.Html5
import qualified Text.Blaze.Html5 as H
import           Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5.Attributes as A
import           Text.Blaze.Html.Renderer.Text (renderHtml)
import qualified Web.Scotty as W (html)

defaultedTemplate bodyContent = do
    W.html $ template "Reading list"
                      "Keeping track of my shit, man"
                      "@bishboria"
                      "Reading list"
                      (defaultNavbarContent "Reading list" "" "")
                      defaultSidebarContent
                      bodyContent

defaultSidebarContent = do
    H.div ! class_ "span3" $ H.div ! class_ "well sidebar-nav" $ do
        ul ! class_ "nav nav-list" $ do
            li $ a ! href "/add" $ "Add new reading material"

defaultNavbarContent brandContent authenticationStatus username = do
    H.div ! class_ "navbar navbar-inverse navbar-fixed-top" $ H.div ! class_ "navbar-inner" $ H.div ! class_ "container-fluid" $ do

        a ! class_ "btn btn-navbar collapsed" ! dataAttribute "target" ".nav-collapse" ! dataAttribute "toggle" "collapse" $ do
            H.span ! class_ "icon-bar" $ ""
            H.span ! class_ "icon-bar" $ ""
            H.span ! class_ "icon-bar" $ ""
        a ! href "/" ! class_ "brand" $ brandContent

        H.div ! class_ "nav-collapse collapse" $ do
            p ! class_ "navbar-text pull-right" $ do
                authenticationStatus
                a ! href "#" ! class_ "navbar-link" $ username

            ul ! class_ "nav" $ do
                li $ a ! href "/about" $ "About"
                li $ a ! href "/contact" $ "Contact"

template :: Html -> AttributeValue -> AttributeValue -> Html -> Markup -> Markup -> Markup -> L.Text
template titleContent metaDescriptionContent authorContent brandContent navbarContent sidebarContent bodyContent = renderHtml
    $ docTypeHtml ! A.lang "en" $ do
        H.head $ do
            meta ! charset "utf-8"
            H.title titleContent
            meta ! content "width=device-width, initial-scale=1.0, maximum-scale=1.0" ! name "viewport"
            meta ! content metaDescriptionContent ! name "description"
            meta ! content authorContent ! name "author"
            link ! rel "stylesheet" ! href "css/bootstrap.min.css"
            H.style ! type_ "text/css" $ "body { padding-top: 60px; padding-bottom: 40px; }\n.sidebar-nav { padding: 9px 0; }"
            link ! rel "stylesheet" ! href "css/bootstrap-responsive.min.css"
            link ! rel "stylesheet" ! href "css/site.css"

        H.body $ do
            navbarContent

            H.div ! class_ "container-fluid" $ H.div ! class_ "row-fluid" $ do
                sidebarContent
                bodyContent

                hr

                H.footer ! class_ "footer" $ p $ do
                    "© "
                    brandContent
                    " 2013"

        mapM_ asset jsFiles

asset :: String -> Html
asset srcURL = script ! src (toValue $ pathPrefix srcURL) $ ""

pathPrefix :: String -> String
pathPrefix (path :: String) =  "js/" ++ path

jsFiles =
    [ "jquery-1.9.0.min.js"
    , "bootstrap.min.js"
    ]
