module Routes
    (
      siteRoutes
    ) where

import Home (homeR)
import AddMaterial (addMaterialR)

siteRoutes dbConn = do
    homeR dbConn
    addMaterialR dbConn
