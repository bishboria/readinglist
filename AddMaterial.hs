{-# LANGUAGE OverloadedStrings #-}

module AddMaterial where

import Web.Scotty
import Text.Blaze.Html5 (toMarkup)

import Databas
import AddMaterialView

addMaterialR dbConn = do
    let addToDB = insertToDB dbConn

    post "/add" $ do
        title <- param "title"
        author <- param "author"
        link <- param "link"
        addToDB $ ReadingMaterial title author link
        addPost (toMarkup title) (toMarkup author) (toMarkup link)

    get "/add" $ do
        addGet
