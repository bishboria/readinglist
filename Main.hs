{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Scotty
import Data.Monoid (mconcat)
import Data.Text.Lazy (pack)

import Databas
import Config
import Middleware (setServerPolicy)
import Routes (siteRoutes)

main :: IO ()
main = do
    config <- getConfigFile
    dbConn <- connectToDB config

    scotty (bind_port config) $ do

        setServerPolicy

        siteRoutes dbConn
