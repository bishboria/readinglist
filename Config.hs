{-# LANGUAGE OverloadedStrings #-}

module Config
    (
      RLConfig(..)
    , getConfigFile
    ) where

import Data.Configurator
import Data.ByteString
import System.Environment

data RLConfig = RLConfig
    { bind_port   :: Int
    , databas     :: ByteString
    , connections :: Int
    } deriving (Show)

getConfigFile :: IO RLConfig
getConfigFile = do
    file <- getArgs
    rlConfig file

rlConfig :: [String] -> IO RLConfig
rlConfig files = do
    cfg <- load $ fmap Required files
    p <- require cfg "bind_port"   :: IO Int
    d <- require cfg "databas"     :: IO ByteString
    c <- require cfg "connections" :: IO Int

    return $ RLConfig p d c
