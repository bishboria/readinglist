{-# LANGUAGE OverloadedStrings #-}

module Home
    (
      homeR
    ) where

import Web.Scotty
import Data.Monoid (mconcat)
import Data.Text.Lazy (pack)

import Databas
import HomeView

homeR dbConn =
    get "/" $ do
        rs <- allReadingMaterial dbConn
        homeGet rs
