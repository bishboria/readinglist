{-# LANGUAGE OverloadedStrings #-}

module Middleware
    (
      setServerPolicy
    ) where

import Web.Scotty
import Network.Wai.Middleware.Static
import Network.Wai.Middleware.RequestLogger

setServerPolicy :: ScottyM ()
setServerPolicy = do
    middleware logStdoutDev
    middleware $ staticPolicy (noDots >-> addBase "static")
