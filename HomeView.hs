{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module HomeView
    (
      homeGet
    ) where

import           Text.Blaze.Html5
import qualified Text.Blaze.Html5 as H
import           Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5.Attributes as A
import           Text.Blaze.Html.Renderer.Text (renderHtml)

import           DefaultTemplateView
import qualified Databas as D

homeGet rs = defaultedTemplate $
    H.div ! class_ "span9" $ do
        H.table ! class_ "table table-striped" $ do
            H.caption $ do
                "Currently listed reading material"
            H.thead $ do
                  H.tr $ do
                    H.th "Title"
                    H.th "Author"
                    H.th "Link"
            H.tbody $ do
              mapM_ renderReadingMaterial rs

renderReadingMaterial r = do
    H.tr $ do
        H.td ! class_ "readingmaterial-title"  $ toMarkup $ D.title r
        H.td ! class_ "readingmaterial-author" $ toMarkup $ D.author r
        H.td ! class_ "readingmaterial-link"   $ toMarkup $ D.link r
