{-# LANGUAGE OverloadedStrings #-}

module Databas
    (
      ReadingMaterial(..)
    , connectToDB
    , allReadingMaterial
    , insertToDB
    ) where

import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple
import Control.Applicative ((<$>), (<*>))
import Control.Monad.IO.Class (liftIO, MonadIO)
import Data.Int
import Config (RLConfig(..))

data ReadingMaterial = ReadingMaterial
    { title  :: String
    , author :: String
    , link   :: String
    }

instance FromRow ReadingMaterial where
    fromRow = ReadingMaterial <$> field <*> field <*> field

instance Show ReadingMaterial where
    show (ReadingMaterial t a l) = "Title: "  ++ t ++ "\n" ++
                                   "Author: " ++ a ++ "\n" ++
                                   "Link: "   ++ l ++ "\n"

connectToDB :: RLConfig -> IO Connection
connectToDB = connectPostgreSQL . databas

allReadingMaterial :: Control.Monad.IO.Class.MonadIO m
                   => Connection
                   -> m [ReadingMaterial]
allReadingMaterial conn = liftIO $ query_ conn allQuery

allQuery :: Query
allQuery = "select title,author,link from \"ReadingMaterial\""

insertToDB :: MonadIO m => Connection -> ReadingMaterial -> m Int64
insertToDB conn r = liftIO $ execute conn insertStatement $ toStringList r

insertStatement :: Query
insertStatement = "insert into \"ReadingMaterial\" (title,author,link) values (?,?,?)"

toStringList :: ReadingMaterial -> [String]
toStringList (ReadingMaterial t a l) = [t,a,l]
